cmake_minimum_required(VERSION 3.0)

# project name init
project(searun
		VERSION 1.0
		DESCRIPTION "Searun - compile and run C programs easily with just a single command."
		LANGUAGES C)

# setting default build type
set(CMAKE_C_STANDARD 11)
set(default_buid_type "Release")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/bin)

# finding all headers and sources 
file(GLOB_RECURSE SOURCES ${PROJECT_SOURCE_DIR}/src/*.c)
file(GLOB_RECURSE HEADERS ${PROJECT_SOURCE_DIR}/inc/*.h)

# include directories
include_directories(${PROJECT_SOURCE_DIR}/inc/)

# building the executable
# add_library(${PROJECT_NAME} STATIC ${SOURCES} ${HEADERS})
add_executable(${PROJECT_NAME} ${SOURCES} ${HEADERS})

#message("Installing the library to lib/...")
# install(TARGETS ${PROJECT_NAME}
# 		RUNTIME DESTINATION ${RUNTIME_OUTPUT_DIRECTORY})




