# Searun

Easily compile and run your small C programs with just one command.

# Usage
Searun creates a `.temp_searun/` folder in your project where it puts all binary files. 
```
=========================================================
searun  v1.1 -- auto compile and run your C programms.
---------------------------------------------------------
USAGE:
        searun [source files] [lfags]

OPTION:
        clean   delete the temp folder

EXAMPLE:
        searun main.c test.c -lmylib 	// compile and run
        searun clean					// delete .temp_searun folder
=========================================================
```

# Building Searun
Clone this repository to your system and execute the `build.sh` shell script. You must have CMake installed on your system.
```
chmod +x build.sh
./build.sh
``` 

Alternatively, you can do the following:
```
cd build
cmake -S .. -B .
make
```

The binary `searun` is placed into the `bin/` folder. Add it to your path.
