#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define CMD_MAX_LENGTH 2048

#if _WIN32 || _WIN64
	#define PATH_SEPARATOR "\\\\"
	#define CMD_DELETE_FOLDER "rmdir /Q /S .temp_searun"
#else
	#define PATH_SEPARATOR "/"
	#define CMD_DELETE_FOLDER "rm -rf .temp_searun"
#endif

#define CMD_CREATE_FOLDER "mkdir -p .temp_searun"

int buildApp(int argc, char const *argv[]);
int runApp(const char *const name);

const char *const manual = "\
=========================================================\n\
searun  v1.2 -- automatically run C programms.\n\
---------------------------------------------------------\n\
USAGE:\n\
	searun [source files] [lfags]\n\
\n\
OPTION:\n\
	clean\tdelete the temp folder\n\
\n\
EXAMPLE:\n\
	searun main.c test.c -lmylib\n\
	searun clean\n\
=========================================================\n\
";

int main(int argc, char const *argv[]) {
	if(argc < 2) {
		printf("%s\n", manual);
		return 0;
	}

	// delete the temp folder if 'clean' command is passed
	const char *const cmdClean = "clean";
	if(strncmp(argv[1], cmdClean, strlen(cmdClean)) == 0) {
		if(system(CMD_DELETE_FOLDER) == 0) {
			printf("\n#searun: removing .temp_searun/\
					\n#searun: DONE\n\n");
		} else {
			printf("\n#searun: error! unable to modify the directory tree!\n");
		}
		
		return 0;
	}

	// count total command length
	size_t userCommandLength = 0;
	for(size_t i = 1; i < argc; i++) {
		userCommandLength += strlen(argv[i]);
	}

	// check user command length
	if(userCommandLength >= CMD_MAX_LENGTH - 512) {
		printf("\n#searun: too many arguments passed.\
				\n#searun: searun is meant to run only small C programs.\
				\n#searun: please, use an automation tool for bigger projects.\n\n");
		return 0;
	}

	// create temp folder
	if(system(CMD_CREATE_FOLDER) != 0) {
		printf("\n#searun: error! unable to modify the directory tree!\n");
		return 0;
	}

	// build the app
	if(buildApp(argc, argv) != 0) {
		printf("#searun: command error! Please, read the \'gcc\' manual.\n\n");
		return 0;
	}

	// run the app
	if(runApp(argv[1]) != 0) {
		printf("#searun: error! failed to run the executable!\n\n");
		return 0;
	}

	printf("#searun: DONE\n\n");

	return 0;
}

int buildApp(int argc, char const *argv[]) {
	char buildCommand[CMD_MAX_LENGTH] = "";

	// create a build command
	strcat(buildCommand, "gcc -o .temp_searun");			// gcc -o .temp_searun
	strcat(buildCommand, PATH_SEPARATOR);					// gcc -o .temp_searun/
	strncat(buildCommand, argv[1], strlen(argv[1]) - 2); 	// gcc -o .temp_searun/file
	for(size_t i = 1; i < argc; i++) {						// gcc -o .temp_searun/file lfags
		strcat(buildCommand, " ");
		strcat(buildCommand, argv[i]);
	}

	// execute the build command
	printf("\n#searun: BUILDING %s\n", buildCommand);
	int result = system(buildCommand);

	return result;
}

int runApp(const char *const name) {
	char runCommand[CMD_MAX_LENGTH] = "";

	// create a run command
	strcpy(runCommand, ".");							// .
	strcat(runCommand, PATH_SEPARATOR);					// ./
	strcat(runCommand, ".temp_searun");					// ./.temp_searun
	strcat(runCommand, PATH_SEPARATOR);   				// ./.temp_searun/
	strncat(runCommand, name, strlen(name) - 2);		// ./.temp_searun/file

	// execute the run command
	printf("#searun: RUNNING %s\n\n", runCommand);
	int result = system(runCommand); printf("\n");

	return result;
}


